﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Part4Variable : MonoBehaviour {
	int myAge = 34;
	float myHeight = 169.9f;
	float sisterHeight = 155.4f;
	float heightRatio;
	string myFirstName = "Jenny";
	string myLastName = "Ahn";
	string myName;

	void Start(){
		myAge++;
		heightRatio = myHeight / sisterHeight;
		myName = myFirstName + myLastName;
	}

	void Update(){
	}

}