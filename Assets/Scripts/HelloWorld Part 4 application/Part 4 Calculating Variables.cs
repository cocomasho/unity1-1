﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Part4CalculatingVariables : MonoBehaviour {
	public int[] calculate() {
		int a = 7;
		int b = 3;

		//c = a + b
		int c = 7 + 3;

		//d = a - b
		int d = 7 - 3;

		//e = a * b
		int e = 7 * 3;

		//below is to generate the above results.
		int[] ret = new int[] { c, d, e };
		return ret;
	}

	public static void Main(){
		if( new Part4CalculatingVariables().calculate()[0] == 10){
			System.Console.WriteLine ("C는 a와 b의 합에 성공하였습니다. 제출을 눌러봐.");
		}
		else{
			System.Console.WriteLine ("C는 a와 b의 합이어야 합니다. 다시 확인.");
		}
			
	}
	
}
